'use strict';

const resultChecker = require('./ResultChecker')

module.exports.hello = (event, context, done) => {
  resultChecker(done) // Please note that the "done" has 2 parameters (error message, success message)
};
