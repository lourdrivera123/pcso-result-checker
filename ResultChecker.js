// #region Libraries
const AWS = require("aws-sdk")
const moment = require('moment-timezone')
const axios = require('axios')
const HtmlTableToJson = require('html-table-to-json')
const cheerio = require('cheerio')
// #endregion Libraries

// #region Configuration
/** Only enable on local dynamodb
  AWS.config.update({
  region: "ap-southeast-1",
  endpoint: "http://localhost:8000"
}) */
moment.tz.setDefault("Asia/Manila")
// #endregion Configuration

// #region Declarations
const docClient = new AWS.DynamoDB.DocumentClient()
const today = moment().format('M/D/YYYY')
var table = "pcso_results"
const games = [
  'Ultra Lotto 6/58',
  'Grand Lotto 6/55',
  'Superlotto 6/49',
  'Megalotto 6/45',
  'Lotto 6/42',
  '6Digit',
  '4Digit',
  'Suertres Lotto 11AM',
  'Suertres Lotto 4PM',
  'Suertres Lotto 9PM',
  'EZ2 Lotto 11AM',
  'EZ2 Lotto 4PM',
  'EZ2 Lotto 9PM',
]
let currentIndex = null
// #endregion Declarations

// #region Methods
const getGameId = game => games.findIndex(g => g == game)+1

const requestData = () => axios.get('http://www.pcso.gov.ph/SearchLottoResult.aspx')

const saveNewResults = (results, callback) => {
  results.forEach((result, index) => {
    currentIndex = index
    const params = {
      TableName:table,
      Key:{
        "game_id": getGameId(result['LOTTO GAME']),
        "draw_date": today,
      },
      UpdateExpression: "set combinations = :combinations, winners = :winners, jackpot_prize = :jackpot_prize, game = :game",
      ExpressionAttributeValues:{
          ":combinations": result['COMBINATIONS'],
          ":jackpot_prize": result['JACKPOT'],
          ":game": result['LOTTO GAME'],
          ":winners": result['WINNERS']
      },
      ReturnValues:"ALL_NEW"
  }

  docClient.update(params, callback)
  })
}
// #endregion Methods

// #region Body
module.exports = callback => {
  requestData().then(res => {
    const data = (new HtmlTableToJson('<table>'+cheerio('#cphContainer_cpContent_GridView1', res.data).children('tbody').html()+'</table>')).results[0];
    const resultsToday = data.filter(d => d['DRAW DATE'] == today)
    
    if(resultsToday.length == 0) {
      console.log(`No results yet for ${moment().format('M/D/YYYY HH:mm')}`)
      return callback(null, `No results yet for ${moment().format('M/D/YYYY HH:mm')}`)
    } else {
      saveNewResults(resultsToday, (err, data) => {
        err ? console.log(`Unable to update item. Error JSON: ${JSON.stringify(err, null, 2)}`) : console.log(`UpdateItem succeeded: ${JSON.stringify(data, null, 2)}`)
        if (currentIndex == resultsToday.length - 1) {
          console.log('ALL RESULTS ARE PROCESSED')
          return callback(err.stackTrace, 'All results processed')
        }
      })
    }
  }).catch(err => callback(`Something went wrong when fetching results for ${today}, Error trace: ${err}`, null))
  }
// #endregion Body
